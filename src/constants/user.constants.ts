import avatar from "../assets/currentAvatar.png";

export const currentUserId = "current-user";
export const currentUserName = "Current User";
export const currentUserAvatar: string = avatar;
