import { useMemo } from "react";
import { countUsers } from "../helpers/messages.helper";
import Message from "../typing/models/messages/message";
import MessagesInfo from "../typing/models/messages/messages-info";

export default function useMessagesInfo(messages: Message[]) {
	return useMemo<MessagesInfo>(() => {
		const usersCount = countUsers(messages);
		const messagesCount = messages.length;

		const lastMessageDate =
			messagesCount > 0
				? messages[messages.length - 1].createdAt // Messages are sorted
				: null;

		return {
			usersCount,
			messagesCount,
			lastMessageDate,
		};
	}, [messages]);
}
