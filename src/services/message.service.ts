import { v4 } from "uuid";
import { defaultMessagesUrl } from "../constants/api.constants";

import {
	currentUserAvatar,
	currentUserId,
	currentUserName,
} from "../constants/user.constants";

import callWebApi from "../helpers/callWebApi.helper";
import { HttpMethod } from "../typing/http";
import Message from "../typing/models/messages/message";

export async function getMessages(
	endpoint: string = defaultMessagesUrl
): Promise<Message[]> {
	const response = await callWebApi({ endpoint, method: HttpMethod.Get });
	return (await response.json()) as Message[];
}

export async function createMessage(text: string): Promise<Message> {
	// Call Web API

	return {
		id: v4(),
		text,
		userId: currentUserId,
		user: currentUserName,
		avatar: currentUserAvatar,
		createdAt: new Date(),
	};
}

/**
 * @param message Necessary because we don't call API
 */
export async function updateMessage(
	message: Message,
	text: string
): Promise<Message> {
	// Call Web API

	return {
		...message,
		text,
	};
}

/**
 * @param message Necessary because we don't call API
 */
export async function likeMessage(message: Message): Promise<Message> {
	// Call Web API
	let diff: number = 1;

	if (message.likerIds?.length && message.likerIds.includes(currentUserId)) {
		diff = -1;
	}

	return {
		...message,
		likeCount: (message.likeCount ?? 0) + diff,
		likerIds:
			diff > 0
				? [...(message.likerIds ?? []), currentUserId]
				: (message.likerIds ?? []).filter((id) => id !== currentUserId),
	};
}

export async function deleteMessage(_id: string): Promise<void> {
	// Call Web API
}
