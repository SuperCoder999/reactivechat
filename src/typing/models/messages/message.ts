export default interface Message {
	id: string;
	userId: string;
	avatar?: string;
	user: string;
	text: string;
	likeCount?: number;
	likerIds?: string[];
	createdAt: Date;
	editedAt?: Date;
}
