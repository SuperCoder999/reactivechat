import Message from "../typing/models/messages/message";

export function sortMessages(a: Message, b: Message): -1 | 1 {
	return a.createdAt < b.createdAt ? -1 : 1;
}

export function countUsers(messages: Message[]): number {
	const users: Record<string, boolean> = {};

	messages.forEach((message) => {
		users[message.userId] = true;
	});

	return Object.keys(users).length;
}
