import moment from "moment";

export function getMessageDividerText(date: Date): string {
	const now = moment();
	const other = moment(date);
	const diff = now.diff(other, "days");

	if (diff === 0) {
		return "Today";
	} else if (diff === 1) {
		return "Yesterday";
	}

	return other.format("dddd, DD MMMM");
}
