import HttpError from "../exceptions/HttpError";
import { RequestArgs } from "../typing/http";

export default async function callWebApi(args: RequestArgs): Promise<Response> {
	const response: Response = await fetch(args.endpoint, getRequestInit(args));
	await throwIfRequestFailed(response);
	return response;
}

function getRequestInit(args: RequestArgs): RequestInit {
	return {
		method: args.method,
		...(args.body ? { body: JSON.stringify(args.body) } : {}),
	};
}

async function throwIfRequestFailed(response: Response): Promise<void> {
	if (response.ok) {
		return;
	}

	let messageFromBody: string | undefined;

	try {
		const data = await response.json();
		messageFromBody = data.message;
	} catch {
		//
	}

	throw new HttpError(
		response.status,
		messageFromBody ?? response.statusText
	);
}
