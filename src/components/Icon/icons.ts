import edit from "../../assets/icons/edit.svg";
import _delete from "../../assets/icons/delete.svg";
import like from "../../assets/icons/like.svg";

const icons: Record<string, string> = {
	edit,
	delete: _delete,
	like,
};

export type IconName = keyof typeof icons;

export default icons;
