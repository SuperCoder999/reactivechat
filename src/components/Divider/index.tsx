import React from "react";
import classnames from "../../helpers/classnames.helper";
import styles from "./divider.module.scss";

interface Props {
	children: string;
	className?: string;
}

const Divider: React.FC<Props> = ({ children, className }) => {
	return (
		<div className={classnames(styles.divider, className)}>
			<div className={styles.line} />
			<div className={styles.text}>{children}</div>
			<div className={styles.line} />
		</div>
	);
};

export default Divider;
