import React, { useEffect, useRef, useState } from "react";
import classnames from "../../helpers/classnames.helper";
import Message from "../../typing/models/messages/message";
import styles from "./messageInput.module.scss";

interface Props {
	updating?: Message;
	onSubmit: (text: string) => void;
}

const MessageInput: React.FC<Props> = ({ updating, onSubmit }) => {
	const [text, setText] = useState<string>("");
	const textareaRef = useRef<HTMLTextAreaElement | null>(null);
	const trimmedText = text.trim();

	useEffect(() => {
		textareaRef.current?.focus();
	}, []);

	useEffect(() => {
		setText((currentText) => updating?.text ?? currentText);
		textareaRef.current?.focus();
	}, [updating]);

	const submit = () => {
		if (trimmedText) {
			onSubmit(trimmedText);
			setText("");
		}
	};

	return (
		<div className={classnames(styles.input, "message-input")}>
			<textarea
				className={classnames(styles.textField, "message-input-text")}
				onChange={(event) => setText(event.target.value)}
				placeholder="Enter your message..."
				value={text}
				ref={textareaRef}
			></textarea>
			<div
				className={classnames(
					styles.submitButton,
					"message-input-button",
					{ [styles.disabled]: !trimmedText }
				)}
				onClick={submit}
				title={trimmedText ? "Send" : "Enter text first :)"}
			>
				&#127876;
			</div>
		</div>
	);
};

export default MessageInput;
