import React from "react";
import classnames from "../../helpers/classnames.helper";
import MessageModel from "../../typing/models/messages/message";
import GeneralMessage from "../GeneralMessage";
import styles from "./ownMessage.module.scss";

interface Props {
	children: MessageModel;
	updateMessage: () => void;
	deleteMessage: () => void;
}

const OwnMessage: React.FC<Props> = ({
	children: message,
	updateMessage,
	deleteMessage,
}) => {
	return (
		<GeneralMessage
			showAvatar
			className={classnames(styles.ownMessage, "own-message")}
			actions={[
				{
					name: "Edit",
					icon: "edit",
					iconClassName: "message-edit",
					function: updateMessage,
				},
				{
					name: "Delete",
					icon: "delete",
					iconClassName: "message-delete",
					function: deleteMessage,
				},
			]}
		>
			{message}
		</GeneralMessage>
	);
};

export default OwnMessage;
