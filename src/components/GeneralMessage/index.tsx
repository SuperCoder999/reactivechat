import React from "react";
import moment from "moment";
import classnames from "../../helpers/classnames.helper";
import Message from "../../typing/models/messages/message";
import { IconName } from "../Icon/icons";
import Icon from "../Icon";
import styles from "./generalMessage.module.scss";

export interface MessageAction {
	icon: IconName;
	name: string;
	info?: string | number;
	iconClassName?: string;
	function: (message: Message) => void;
}

interface Props {
	showAvatar?: boolean;
	actions?: MessageAction[];
	className?: string;
	children: Message;
}

const GeneralMessage: React.FC<Props> = ({
	children: message,
	showAvatar,
	actions,
	className,
}) => {
	return (
		<div className={classnames(styles.generalMessage, className)}>
			{message.avatar && showAvatar ? (
				<img
					alt="Avatar"
					className={classnames(styles.avatar, "message-user-avatar")}
					src={message.avatar}
				/>
			) : null}
			<div className={styles.texts}>
				<div className={styles.metatext}>
					<span className="message-user-name">{message.user}</span>
					<span className={styles.bullet}>&bull;</span>
					<span className="message-time">
						{moment(message.createdAt).format("HH:mm")}
					</span>
					{message.editedAt ? (
						<>
							<span className={styles.bullet}>&bull;</span>
							edited at {moment(message.editedAt).format("HH:mm")}
						</>
					) : null}
				</div>
				<span className="message-text">{message.text}</span>
				<div className={styles.actions}>
					{(actions ?? []).map((action, index) => (
						<div key={index}>
							<Icon
								hint={action.name}
								className={action.iconClassName}
								onClick={() => action.function(message)}
							>
								{action.icon}
							</Icon>
							{action.info !== undefined ? (
								<div className={styles.info}>{action.info}</div>
							) : null}
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

export default GeneralMessage;
