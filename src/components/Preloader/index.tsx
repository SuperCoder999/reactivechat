import React from "react";
import classnames from "../../helpers/classnames.helper";
import styles from "./preloader.module.scss";

interface Props {
	loading?: boolean;
}

const Preloader: React.FC<Props> = ({ loading }) => {
	if (!loading) {
		return null;
	}

	return (
		<div className={classnames(styles.container, "preloader")}>
			<div className={styles.loader} />
		</div>
	);
};

export default Preloader;
