import moment from "moment";
import React, { useCallback } from "react";
import Divider from "../../components/Divider";
import Message from "../../components/Message";
import OwnMessage from "../../components/OwnMessage";
import classnames from "../../helpers/classnames.helper";
import { getMessageDividerText } from "../../helpers/date.helper";
import MessageModel from "../../typing/models/messages/message";
import styles from "./messageList.module.scss";

interface Props {
	children: MessageModel[];
	currentUserId: string;
	likeMessage: (message: MessageModel) => void;
	updateMessage: (message: MessageModel) => void;
	deleteMessage: (message: MessageModel) => void;
}

const MessageList: React.FC<Props> = ({
	children: messages,
	currentUserId,
	likeMessage,
	updateMessage,
	deleteMessage,
}) => {
	let lastMessageDividerDate: Date | null = null;

	const getMessageElement = useCallback(
		(message: MessageModel) => {
			return message.userId === currentUserId ? (
				<OwnMessage
					key={message.id}
					updateMessage={() => updateMessage(message)}
					deleteMessage={() => deleteMessage(message)}
				>
					{message}
				</OwnMessage>
			) : (
				<Message
					key={message.id}
					likeMessage={() => likeMessage(message)}
				>
					{message}
				</Message>
			);
		},
		[currentUserId, likeMessage, updateMessage, deleteMessage]
	);

	return (
		<div className={classnames(styles.messageList, "message-list")}>
			{messages.map((message) => {
				const element = getMessageElement(message);

				const dateDiff: number | null = lastMessageDividerDate
					? moment(lastMessageDividerDate).diff(
							moment(message.createdAt),
							"days"
					  )
					: null;

				if (!lastMessageDividerDate || Math.abs(dateDiff ?? 0) >= 1) {
					lastMessageDividerDate = message.createdAt;

					return (
						<React.Fragment key={message.id}>
							<Divider className="messages-divider">
								{getMessageDividerText(lastMessageDividerDate)}
							</Divider>
							{element}
						</React.Fragment>
					);
				}

				return element;
			})}
		</div>
	);
};

export default MessageList;
