import React, { useCallback, useEffect, useState } from "react";
import MessageInput from "../../components/MessageInput";
import Preloader from "../../components/Preloader";
import { currentUserId } from "../../constants/user.constants";
import { replaceGlobalEventListener } from "../../helpers/globalEvents.helper";
import { sortMessages } from "../../helpers/messages.helper";

import {
	getMessages,
	createMessage,
	likeMessage,
	updateMessage,
	deleteMessage,
} from "../../services/message.service";

import Message from "../../typing/models/messages/message";
import Header from "../Header";
import MessageList from "../MessageList";
import styles from "./chat.module.scss";

interface Props {
	url?: string;
}

const Chat: React.FC<Props> = ({ url }) => {
	const [loading, setLoading] = useState<boolean>(true);
	const [messages, setMessages] = useState<Message[]>([]);

	const [updatingMessage, setUpdatingMessage] = useState<
		Message | undefined
	>();

	useEffect(() => {
		setLoading(true);

		getMessages(url).then((newMessages) => {
			setMessages(newMessages.sort(sortMessages));
			setLoading(false);
		});
	}, [url]);

	useEffect(() => {
		replaceGlobalEventListener(1, "keyup", (event: Event) => {
			const keyboardEvent = event as KeyboardEvent;

			if (keyboardEvent.code === "ArrowUp") {
				const reversedMessages = [...messages].reverse();

				const lastOwnMessage = reversedMessages.find(
					(message) => message.userId === currentUserId
				); // Messages are sorted by date

				setUpdatingMessage(lastOwnMessage);
			}
		});
	}, [messages]);

	const create = useCallback(
		async (text: string) => {
			const message = await createMessage(text);
			setMessages([...messages, message]);
		},
		[messages]
	);

	const like = useCallback(
		async (id: string) => {
			const messageIndex = messages.findIndex(
				(message) => message.id === id
			); // Necessary because we don't call API

			const message = messages[messageIndex];
			const newMessage = await likeMessage(message);
			const newMessages = [...messages];

			newMessages[messageIndex] = newMessage;
			setMessages(newMessages);
		},
		[messages]
	);

	const update = useCallback(
		async (id: string, text: string) => {
			const messageIndex = messages.findIndex(
				(message) => message.id === id
			); // Necessary because we don't call API

			const message = messages[messageIndex];
			const newMessage = await updateMessage(message, text);
			const newMessages = [...messages];

			newMessages[messageIndex] = newMessage;
			setMessages(newMessages);
			setUpdatingMessage(undefined);
		},
		[messages]
	);

	const remove = useCallback(
		async (id: string) => {
			await deleteMessage(id);

			const messageIndex = messages.findIndex(
				(message) => message.id === id
			);

			const newMessages = [...messages];
			newMessages.splice(messageIndex, 1);

			setMessages(newMessages);
		},
		[messages]
	);

	return (
		<div className="chat">
			<Preloader loading={loading} />
			{!loading ? (
				<div className={styles.container}>
					<Header>{messages}</Header>
					<MessageList
						likeMessage={(message) => like(message.id)}
						updateMessage={(message) => setUpdatingMessage(message)}
						deleteMessage={(message) => remove(message.id)}
						currentUserId={currentUserId}
					>
						{messages}
					</MessageList>
					<MessageInput
						updating={updatingMessage}
						onSubmit={
							updatingMessage
								? (text) => update(updatingMessage.id, text)
								: create
						}
					/>
				</div>
			) : null}
		</div>
	);
};

export default Chat;
