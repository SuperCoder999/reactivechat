import React from "react";
import moment from "moment";
import Statistic from "../../components/Statistic";
import classnames from "../../helpers/classnames.helper";
import useMessagesInfo from "../../hooks/useMessagesInfo.hook";
import Message from "../../typing/models/messages/message";
import styles from "./header.module.scss";

interface Props {
	children: Message[];
}

const Header: React.FC<Props> = ({ children: messages }) => {
	const { usersCount, messagesCount, lastMessageDate } =
		useMessagesInfo(messages);

	return (
		<div className={classnames(styles.header, "header")}>
			<span className={classnames(styles.title, "header-title")}>
				&#127876; Reactive Chat
			</span>
			<div className={styles.metrics}>
				<Statistic label="Users" valueClassName="header-users-count">
					{usersCount}
				</Statistic>
				<Statistic
					label="Messages"
					valueClassName="header-messages-count"
				>
					{messagesCount}
				</Statistic>
				{lastMessageDate ? (
					<span className={styles.lastMessageDate}>
						Last message{" "}
						<span className="header-last-message-date">
							{moment(lastMessageDate).format("DD.MM.yyyy HH:mm")}
						</span>
					</span>
				) : null}
			</div>
		</div>
	);
};

export default Header;
