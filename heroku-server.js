// WARNING: THIS IS A STARTUP FILE FOR HEROKU

const express = require("express");
const path = require("path");

const reactPath = path.join(__dirname, "build");
const app = express();

app.use(express.static(reactPath));
app.get("/status", (_, res) => res.json({ status: "ok" }));

app.listen(process.env.PORT ?? 3000);
